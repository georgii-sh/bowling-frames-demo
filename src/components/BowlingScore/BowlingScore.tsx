import React from 'react';
import ScoreFrame from '../ScoreFrame/ScoreFrame';

interface Props {
  rolls: number[];
}

export function getFrames(rolls: number[]): number[][] | undefined {
  if (rolls.length === 0) {
    return;
  }
  const frames: number[][] = [[]];
  rolls.forEach(roll => {
    const lastFrame = frames[frames.length - 1];
    if (frames.length < 10 && (lastFrame.length > 1 || lastFrame[0] === 10)) {
      frames.push([]);
    }

    frames[frames.length - 1].push(roll);
  });

  return frames;
}

const Scores: React.FC<Props> = ({ rolls }) => {
  const frames = getFrames(rolls);
  return (
    <div css={{ display: 'flex' }}>
      {frames &&
        frames.map((round, i) => (
          <ScoreFrame key={`score-frame-${i}`} round={round} />
        ))}
    </div>
  );
};

export default Scores;
