import React from 'react';
import { render } from '@testing-library/react';
import BowlingScore from './BowlingScore';

describe('BowlingScore', () => {
  it('should not render frames with empty rolls', () => {
    const { queryAllByLabelText } = render(<BowlingScore rolls={[]} />);
    expect(queryAllByLabelText('score-frame')).toHaveLength(0);
  });

  it('should render one frame', () => {
    const { getAllByLabelText } = render(<BowlingScore rolls={[2, 3]} />);
    const frames = getAllByLabelText('score-frame');
    expect(frames).toHaveLength(1);
    expect(frames[0].textContent).toBe('23');
  });

  it('should render two frame', () => {
    const { getAllByLabelText } = render(<BowlingScore rolls={[2, 3, 4, 5]} />);
    const frames = getAllByLabelText('score-frame');
    expect(frames).toHaveLength(2);
    expect(frames[0].textContent).toBe('23');
    expect(frames[1].textContent).toBe('45');
  });

  it('should render two frame both strike', () => {
    const { getAllByLabelText } = render(<BowlingScore rolls={[10, 10]} />);
    const frames = getAllByLabelText('score-frame');
    expect(frames).toHaveLength(2);
    expect(frames[0].textContent).toBe('X');
    expect(frames[1].textContent).toBe('X');
  });

  it('should render two frame strite and spare', () => {
    const { getAllByLabelText } = render(<BowlingScore rolls={[10, 0, 10]} />);
    const frames = getAllByLabelText('score-frame');
    expect(frames).toHaveLength(2);
    expect(frames[0].textContent).toBe('X');
    expect(frames[1].textContent).toBe('-/');
  });

  it('should render 10 frames for perfect game', () => {
    const { getAllByLabelText } = render(
      <BowlingScore rolls={[10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]} />
    );
    const frames = getAllByLabelText('score-frame');
    expect(frames).toHaveLength(10);
    expect(frames[9].textContent).toEqual('XXX');
  });
});
