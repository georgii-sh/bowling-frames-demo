import React from 'react';

import Frame from '../Frame/Frame';
import styles from '../Frame/Frame.styles';

interface Props {
  value: number;
  label: string;
}

const FrameValue: React.FC<Props> = ({ value, label }) => {
  const renderPins = (pins: number): string | number => {
    if (pins === 0) {
      return '-';
    }
    if (pins === 10) {
      return 'X';
    }
    return pins;
  };
  return (
    <div css={styles.value} aria-label={label}>
      {renderPins(value)}
    </div>
  );
};

const SpareFrame: React.FC = () => (
  <div css={styles.value} aria-label="second">
    /
  </div>
);

const ScoreFrame: React.FC<{ round: number[] }> = ({ round }) => {
  const [first, second, third] = round;
  return (
    <Frame>
      <FrameValue value={first} label="first" />
      {first + second === 10 ? (
        <SpareFrame />
      ) : (
        <FrameValue value={second} label="second" />
      )}
      {third !== undefined && <FrameValue value={third} label="third" />}
    </Frame>
  );
};

export default ScoreFrame;
