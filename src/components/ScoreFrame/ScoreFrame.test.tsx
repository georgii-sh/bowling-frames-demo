import React from 'react';
import { render } from '@testing-library/react';
import ScoreFrame from './ScoreFrame';

describe('ScoreFrame', () => {
  it('should render 2, 3 correct', () => {
    const { getByLabelText } = render(<ScoreFrame round={[2, 3]} />);
    expect(getByLabelText('first').textContent).toBe('2');
    expect(getByLabelText('second').textContent).toBe('3');
  });

  it('should render 0, 1 correct', () => {
    const { getByLabelText } = render(<ScoreFrame round={[0, 1]} />);
    expect(getByLabelText('first').textContent).toBe('-');
    expect(getByLabelText('second').textContent).toBe('1');
  });

  it('should render 2, 0 correct', () => {
    const { getByLabelText } = render(<ScoreFrame round={[2, 0]} />);
    expect(getByLabelText('first').textContent).toBe('2');
    expect(getByLabelText('second').textContent).toBe('-');
  });

  it('should render spare correct', () => {
    const { getByLabelText } = render(<ScoreFrame round={[2, 8]} />);
    expect(getByLabelText('first').textContent).toBe('2');
    expect(getByLabelText('second').textContent).toBe('/');
  });

  it('should render strike correct', () => {
    const { getByLabelText } = render(<ScoreFrame round={[10]} />);
    expect(getByLabelText('first').textContent).toBe('X');
    expect(getByLabelText('second').textContent).toBe('');
  });

  it('should render miss correct', () => {
    const { getByLabelText } = render(<ScoreFrame round={[0, 0]} />);
    expect(getByLabelText('first').textContent).toBe('-');
    expect(getByLabelText('second').textContent).toBe('-');
  });

  it('should render three strike correct', () => {
    const { getByLabelText } = render(<ScoreFrame round={[10, 10, 10]} />);
    expect(getByLabelText('first').textContent).toBe('X');
    expect(getByLabelText('second').textContent).toBe('X');
    expect(getByLabelText('third').textContent).toBe('X');
  });

  it('should render three spare and miss correct', () => {
    const { getByLabelText } = render(<ScoreFrame round={[1, 9, 0]} />);
    expect(getByLabelText('first').textContent).toBe('1');
    expect(getByLabelText('second').textContent).toBe('/');
    expect(getByLabelText('third').textContent).toBe('-');
  });

  it('should not render third for 2 round frame', () => {
    const { queryByLabelText } = render(<ScoreFrame round={[1, 9]} />);
    expect(queryByLabelText('third')).not.toBeInTheDocument();
  });
});
