import React, { ReactNode } from 'react';

import styles from './Frame.styles';

interface Props {
  children?: ReactNode;
}

const Frame: React.FC<Props> = ({ children }) => {
  return (
    <div css={styles.frame} aria-label="score-frame">
      <div css={styles.values}>{children}</div>
      <div></div>
    </div>
  );
};

export default Frame;
