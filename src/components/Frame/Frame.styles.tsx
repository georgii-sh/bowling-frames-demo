import { InterpolationWithTheme } from '@emotion/core';

export default {
  frame: {
    border: '1px solid',
    textAlign: 'center',
    lineHeight: '30px',
    margin: '0 5px',
    width: 100,
    height: 80,
    overflow: 'hidden',
  } as InterpolationWithTheme<string>,
  values: {
    display: 'flex',
    justifyContent: 'flex-end',
  } as InterpolationWithTheme<string>,
  value: {
    width: 30,
    height: 30,
    borderBottom: '1px solid',
    borderLeft: '1px solid',
  } as InterpolationWithTheme<string>,
};
