import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

import theme from './theme';
import BowlingScore from './components/BowlingScore/BowlingScore';

const App: React.FC = () => {
  return (
    <>
      <CssBaseline />
      <Container css={{ paddingTop: theme.spacing(8), textAlign: 'center' }}>
        <Typography variant="h3">React bowling score</Typography>
        <div css={{ paddingTop: theme.spacing(4), display: 'inline-flex' }}>
          <BowlingScore
            rolls={[6, 2, 7, 2, 3, 4, 8, 2, 9, 0, 10, 10, 10, 6, 3, 10, 8, 1]}
          />
        </div>
      </Container>
    </>
  );
};

export default App;
